import datetime
from typing import List, Dict, Optional

from cryton_e2e.lib.tests import TestRunExecution
from cryton_e2e.lib.util import helpers


class TestDatetimeTriggerExecution(TestRunExecution):
    """
    Datetime trigger Run execution testing.
    """
    def __init__(self, template: str, workers: List[Dict], inventories: Optional[List[str]],
                 execution_variables: Optional[List[str]], max_timeout: int):
        super().__init__(template, workers, inventories, execution_variables, max_timeout)
        self.description = f"HTTP trigger test: {self.description}"

        start_time = datetime.datetime.utcnow() + datetime.timedelta(seconds=5)
        inventory = {"dt_trigger": {"timezone": "UTC", "hour": start_time.hour, "minute": start_time.minute,
                                    "second": start_time.second}}
        self.inventories.append(helpers.create_inventory(inventory))
