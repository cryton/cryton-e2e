FROM registry.gitlab.ics.muni.cz:443/cryton/cryton-cli:1 as base

RUN pip install pipx
RUN pipx install poetry
RUN ln -s /root/.local/bin/poetry /usr/local/bin/poetry

ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV CRYTON_E2E_WORKER_ADDRESS=192.168.90.11
ENV TZ=UTC

RUN apk add --no-cache \
    curl

WORKDIR /e2e

# Install dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry install --without dev --no-root --no-interaction --no-ansi

# Install app
COPY . /e2e/
RUN poetry install --only-root --no-interaction --no-ansi

RUN poetry install

RUN ln -s /e2e/.venv/bin/cryton-e2e /usr/local/bin/cryton-e2e
